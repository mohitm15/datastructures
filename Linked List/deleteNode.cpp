#include <bits/stdc++.h> 
using namespace std; 

class Node{
	public:
	int data;
	Node *next;
};

void insertAtFront(Node **head_ref,int newdata)
{
	Node *newNode = new Node();
	newNode->data = newdata;
	newNode->next = *(head_ref);
	*(head_ref) = newNode;
}

void insertAtIndex(Node **head_ref,int newdata,int index)
{
	int cnt=0;
	Node *newNode = new Node();
	Node *travel = *(head_ref);
	newNode->data = newdata;
	if(*head_ref == NULL)
	{
		*head_ref = newNode;
		return;
	}
	while(1)
	{
		if(cnt ==(index-2))
		break;
		travel = travel->next;
		cnt++;
		//cout<<"yo";
	}
	newNode->next = travel->next;
	travel->next = newNode;
}

void insertAtLast(Node **head_ref,int newdata)
{
	Node *newNode = new Node();
	Node *last = *head_ref;
	newNode->data = newdata;
	newNode->next =NULL;
	if(*(head_ref)==NULL)
	{
		*head_ref = newNode;
		return;
	}
	while(last->next!=NULL)
	{
		last=last->next;
	}
	last->next =newNode;
	return;
}

void deleteNode(Node **head_ref, int key)
{
	Node *traverse = *(head_ref),*prev;

	if(traverse->data == key && traverse != NULL)
	{
		*(head_ref)= traverse->next;
		traverse->next = NULL;
		free(traverse);
		return;
	}
	while(traverse->data != key)
	{
		prev =traverse;
		traverse = traverse->next;
		//cout<<"uo";
	}
	prev->next = traverse->next;
	traverse->next = NULL;
	return;
}

void printList(Node *n)
{
	while(n!=NULL)
	{
		cout<<n->data<<"->";
		n=n->next;
	}
	cout<<endl;
}


/* Driver code*/
int main() 
{ 
	Node *head=NULL;
	int v1,value,v2,v3,v4,v5,delkey;
	cin>>v1>>v2>>v3>>v4>>v5;
	insertAtLast(&head, v1);
	insertAtLast(&head, v2);
	insertAtLast(&head, v3);
	insertAtLast(&head, v4);
	insertAtLast(&head, v5);
	printList(head);
	cin>>value;
	insertAtIndex(&head, value,3);
	printList(head);
	cin>>delkey;
	deleteNode(&head,delkey);
	printList(head);
	
	return 0; 
} 


