#include<bits/stdc++.h>
using namespace std;

class Node {
    public:
    int data;
    Node *prev;
    Node *next;
};

void insertAtFirst(Node **head_ref,int newdata)
{
    Node *newNode = new Node();
    newNode->data =newdata;
    newNode->next=(*head_ref);
    newNode->prev =NULL;
    if ((*head_ref) != NULL)  
        (*head_ref)->prev = newNode;  
  
    (*head_ref) = newNode; 
}
void insertAtLast(Node **head_ref,int newdata)
{
    Node* newNode = new Node();
    Node* last = (*head_ref);
    newNode->data = newdata;
    newNode->next = NULL;
    if((*head_ref)== NULL)
    {
        newNode->prev = NULL;
        *head_ref = newNode;
        return;
    }
    while(last->next != NULL)
     last= last->next;

     last->next = newNode;
     newNode->prev = last;   
}


void printList(Node *n)
{
	while(n!=NULL)
	{
		cout<<n->data<<"->";
		n=n->next;
	}
	cout<<endl;
}

int main(){
    int v1;
    Node *head = NULL;
    Node *tail = NULL;
    insertAtLast(&head,12);
    printList(head);
    insertAtLast(&head,11);
    printList(head);
    insertAtLast(&head,10);
    printList(head);
    return 0;
}