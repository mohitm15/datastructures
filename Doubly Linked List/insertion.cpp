//Insertion at front
//Insertion at end
//Insertion at middle

//By mohit maroliya
#include<bits/stdc++.h>
using namespace std;

class Node {
    public:
    int data;
    Node *prev;
    Node *next;
};

void insertAtFirst(Node **head_ref,int newdata)
{
    Node *newNode = new Node();
    newNode->data =newdata;
    newNode->next=(*head_ref);
    newNode->prev =NULL;
    if ((*head_ref) != NULL)  
        (*head_ref)->prev = newNode;  
  
    (*head_ref) = newNode; 
}

void insertAtIndex(Node **head_ref, int newdata, int index)
{
    int cnt=0;
    Node *newNode = new Node();
    Node *travel = *head_ref;
    newNode->data = newdata;
    while(1)
    {
        //cout<<"yo";
        if(cnt ==(index-2))
		break;
        travel = travel->next;
        cnt++;
    }
    newNode->next = travel->next;
    travel->next = newNode;
    newNode->prev = travel;

    if(newNode->next == NULL)
    {
        newNode->next->prev = newNode;
    }
}
void insertAtLast(Node **head_ref,int newdata)
{
    Node* newNode = new Node();
    Node* last = (*head_ref);
    newNode->data = newdata;
    newNode->next = NULL;
    if((*head_ref)== NULL)
    {
        newNode->prev = NULL;
        *head_ref = newNode;
        return;
    }
    while(last->next != NULL)
     last= last->next;

     last->next = newNode;
     newNode->prev = last;   
}


void printList(Node *n)
{
	while(n!=NULL)
	{
		cout<<n->data<<"->";
		n=n->next;
	}
	cout<<endl;
}

int main(){
    int v1;
    Node *head = NULL;
    Node *tail = NULL;
    insertAtLast(&head,15);
    insertAtLast(&head,14);
    insertAtLast(&head,13);
    insertAtLast(&head,12);
    insertAtLast(&head,11);
    printList(head);
    cin>>v1;
    insertAtIndex(&head,v1,3);
    printList(head);
    return 0;
}