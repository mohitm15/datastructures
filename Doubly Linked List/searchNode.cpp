#include<bits/stdc++.h>
using namespace std;

class Node {
    public:
    int data;
    Node *prev;
    Node *next;
};

void insertAtFirst(Node **head_ref,int newdata)
{
    Node *newNode = new Node();
    newNode->data =newdata;
    newNode->next=(*head_ref);
    newNode->prev =NULL;
    if ((*head_ref) != NULL)  
        (*head_ref)->prev = newNode;  
  
    (*head_ref) = newNode; 
}

void insertAtIndex(Node **head_ref, int newdata, int index)
{
    int cnt=0;
    Node *newNode = new Node();
    Node *travel = *head_ref;
    newNode->data = newdata;
    while(1)
    {
        //cout<<"yo";
        if(cnt ==(index-2))
		break;
        travel = travel->next;
        cnt++;
    }
    newNode->next = travel->next;
    travel->next = newNode;
    newNode->prev = travel;

    if(newNode->next == NULL)
    {
        newNode->next->prev = newNode;
    }
}
void insertAtLast(Node **head_ref,int newdata)
{
    Node* newNode = new Node();
    Node* last = (*head_ref);
    newNode->data = newdata;
    newNode->next = NULL;
    if((*head_ref)== NULL)
    {
        newNode->prev = NULL;
        *head_ref = newNode;
        return;
    }
    while(last->next != NULL)
     last= last->next;

     last->next = newNode;
     newNode->prev = last;   
}
void searchNode(Node **head_ref,int key)
{
    Node *current = *head_ref;
    while(current->next !=NULL)
    {
        if(current->data == key)
        {
            cout<<"founded"<<endl;
            return;
        }
        else
        current=current->next;
    }
        cout<<"Not FOund"<<endl;
        return;
}
void deleteNode(Node **head_ref,int key)
{
    Node *target = *head_ref;
    while(target->data!= key)
    {
        target = target->next;
    }
    if (*head_ref == NULL || target == NULL) 
        return; 
  
    if (*head_ref == target) 
        *head_ref = target->next; 
  
    if (target->next != NULL) 
        target->next->prev = target->prev; 
  
    if (target->prev != NULL) 
        target->prev->next = target->next;
    
}

void printList(Node *n)
{
	while(n!=NULL)
	{
		cout<<n->data<<" <-> ";
		n=n->next;
	}
	cout<<endl;
}
void printReverse(Node **head_ref)
{
    Node *tail = *head_ref;
    while(tail->next!=NULL)
    tail= tail->next;
    while(tail!= *head_ref)
    {
        cout<<tail->data<<"<-";
        tail=tail->prev;
    }
    cout<<tail->data<<endl;
}
int main(){
    int v1;
    Node *head = NULL;
    Node *tail = NULL;
    insertAtLast(&head,15);
    insertAtLast(&head,14);
    insertAtLast(&head,13);
    insertAtLast(&head,12);
    insertAtFirst(&head,11);
    printList(head);
    cin>>v1;
    insertAtIndex(&head,v1,4);
    printList(head);
    searchNode(&head,v1);
    // printList(head);
    //printReverse(&head);
    return 0;
}